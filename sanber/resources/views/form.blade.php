<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Form</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="welcome">
            <label>First name:</label> <br><br>
            <input type="text" name="nama"> <br><br>
            <label>Last name:</label> <br><br>
            <input type="text" name="nama2"> <br><br>
            <label>Gender:</label> <br><br>
                <input type="radio" name="gender">Male <br><br>
                <input type="radio" name="gender">Female <br><br>
                <input type="radio" name="gender">Other <br><br>
            <label>Nationality:</label> <br><br>
            <select name="Nationality">
               <option value="Indonesia">Indonesia</option>
               <option value="Prancis">Prancis</option>
               <option value="Amerika">Amerika</option>
           </select> <br><br>
           <label>Language Spoken:</label> <br><br>
           <input type="checkbox"> Bahasa Indonesia <br><br>
           <input type="checkbox"> English <br><br>
           <input type="checkbox"> Other <br><br>
           <label>Bio:</label> <br><br>
           <textarea name="bio" cols="20" row="60"></textarea> <br><br>
           <button type="submit"> Sign Up </button>
        </form>
    </body>
</html>
